#!/bin/bash
ds=0
dsr=0
read -p "Dame un numero del mes entre 1 y 30: " d
while [ $d -lt 1 -o $d -gt 30 ];
do
	read -p "Dame un numero valido entre 1 y 30: " d
done
ds=$(expr $d / 7)
dsr=$(expr $d - 7 \* $ds)
if [ $dsr -eq 1 ]; then
	echo "Es lunes"
elif [ $dsr -eq 2 ]; then
	echo "Es martes"
elif [ $dsr -eq 3 ]; then
	echo "Es miércoles"
elif [ $dsr -eq 4 ]; then
	echo "Es jueves"
elif [ $dsr -eq 5 ]; then
	echo "Es viernes"
elif [ $dsr -eq 6 ]; then
	echo "Es sabado"
else
	echo "Es domingo"
fi
