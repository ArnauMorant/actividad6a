#!/bin/bash
read -p "Dime una nota " n
while [ $n -gt 10 -o $n -lt 0 ];
do
	read -p "Dime una nota valida " n
done
if [ $n -ge 0 -a $n -le 4 ]; then
	echo "Suspenso"
elif [ $n = 5 ]; then
	echo "Aprobado"
elif [ $n = 6 ]; then
	echo "Bien"
elif [ $n = 7 -o $n = 8 ]; then
	echo "Notable"
elif [ $n = 9 -o $n = 10 ]; then
	echo "Sobresaliente"
fi
