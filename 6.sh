#!/bin/bash
read -p "Dame los litros que has consumido " l
if [ $l -le 50 ]; then
	echo "El precio total es 20 €"
elif [ $l -gt 50 -a $l -le 200 ]; then
    l=$(expr $l - 50)
	r=$( echo "$l * 0.20 + 20" | bc -l)
    echo "El precio total es $r €"
elif [ $l -gt 200 ]; then
	l=$(expr $l - 200)
	r=$( echo "$l * 0.10 + 50" | bc -l)
	echo "El precio total es $r €"
fi
